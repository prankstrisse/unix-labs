#!/usr/bin/awk
BEGIN {
	weekdays[1] = "Mon"
    weekdays[2] = "Tue"
    weekdays[3] = "Wed"
    weekdays[4] = "Thu"
    weekdays[5] = "Fri"
    weekdays[6] = "Sat"
    weekdays[7] = "Sun"

    printf "client\t\t\t"
    for(i=1; i<=7; ++i)
    	printf "%s\t", weekdays[i]
    
    printf "sum\n"
}

{
    "date --date=\""$1" "$2"\" +%a" | getline day
    close("date --date=\""$1" "$2"\" +%a")
    days[day] = 1
    clients[$5] = 1
    arr[day,$5]++
}

END {
    for (b in clients) {
    	printf "%s\t", b
    	sum = 0
    	for(i=1; i<=7; ++i) {
    		if (weekdays[i] in days) {
    			sum += arr[weekdays[i],b]
    			if (arr[weekdays[i], b] == "")
    				printf "--\t"
    			else	
	    			printf "%s\t", arr[weekdays[i], b]
    		} else {
    			printf "--\t"	
    		}    		
    	}
    	printf "%s\n", sum
    }
    print "\n"
}

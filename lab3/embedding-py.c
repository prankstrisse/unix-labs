#include <Python.h>
#include <stdio.h>
//gcc $(python-config --includes) $(python-config --ldflags) embedding-py.c 
int main()
{
  
  long a = 3;
  long b = 10;

  Py_Initialize();

  /*
  PyRun_SimpleString("import sys; sys.path.append('.')");
  PyRun_SimpleString("import rectangle;");
  PyRun_SimpleString("print rectangle.square(2, 4)");
  PyRun_SimpleString("print rectangle.perimeter(2, 4)");
  */

  //importing a module
  PyRun_SimpleString("import sys; sys.path.append('.')");
  PyObject* moduleString = PyString_FromString((char*)"rectangle");
  PyObject* module = PyImport_Import(moduleString);

  PyObject* args = PyTuple_Pack(2, PyInt_FromLong(a), PyInt_FromLong(b));


  //getting a reference to perimeter function
  PyObject* perimeterFunction = PyObject_GetAttrString(module, "perimeter");
  PyObject* perimeterResult = PyObject_CallObject(perimeterFunction, args);
  long perimeter = PyInt_AsLong(perimeterResult);
  printf("perimeter: %d\n", perimeter);


  PyObject* squareFunction = PyObject_GetAttrString(module, "square");
  PyObject* squareResult = PyObject_CallObject(squareFunction, args);
  long square = PyInt_AsLong(squareResult);
  printf("square: %d\n", square);

  Py_Finalize();
  return 0;
}
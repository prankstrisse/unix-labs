#ifdef __cplusplus
extern "C" {
#endif 
#include <Python.h>
#include <stdio.h>
//gcc $(python-config --includes) $(python-config --ldflags) -shared c4py.c -o rectangle

static PyObject* perimeterCmd(PyObject* self, PyObject* val) {
  int a, b, p;
  if (!PyArg_ParseTuple(val, "ii", &a, &b)) {
    return NULL;
  }
  p = 2 * (a + b);
  return Py_BuildValue("i", p);

}

static PyObject* squareCmd(PyObject* self, PyObject* val) {
  int a, b, s;
  if (!PyArg_ParseTuple(val, "ii", &a, &b)) {
    return NULL;
  }
  s = a * b;
  return Py_BuildValue("i", s);

}

static PyMethodDef rectangleMethods[] = {
  {"perimeter", perimeterCmd, METH_VARARGS, "Calculate perimeter"}, 
  {"square", squareCmd, METH_VARARGS, "Calculate square"}, 
  {NULL, NULL, 0, NULL}
};

PyMODINIT_FUNC initrectangle() {
  (void) Py_InitModule("rectangle", rectangleMethods);
}

